# Tramhuonggiabao

Trầm hương An Gia Bảo với nhiều năm kinh nghiệm phân phối các sản phẩm trầm hương cao cấp như vòng trầm hương, nụ trầm, trầm cảnh…

- Địa chỉ: Nguyễn Thái Sơn, P5, Gò Vấp, TPHCM

- SDT: 0896565123

Cây trầm hương bản chất là tên 1 tên gọi khác cây Dó Bầu – loại cây mang thể sinh ra gỗ trầm. Theo khoa học thì loại cây này mang tên là Aquilaria agallocha Roxb. Ngoài ra nó còn được biết tới mang một số tên khác như: cây Dó Trầm, cây Dó, cây Trầm, cây Kỳ Nam…

Cây này khá là thích hợp sở hữu mẫu đất feralit. Vì thế nó xuất hiện hơi phổ quát trong các rừng nguyên sinh hoặc thứ sinh, ở những nước nhiệt đới, đặc biệt là khu vực Đông Nam Á như Việt Nam, Lào, Campuchia. Tại nước ta, chiếc cây này có mặt trong khoảng Hà Giang cho đến Phú Quốc. Đặc biệt là vùng miền trung, cây trầm hương phát triển khá rẻ.

Nó là một chiếc cây thân gỗ lớn, cao trong khoảng 15-30m, thỉnh thoảng với thể cao lên tới 40m. Lá cây mang hình bầu dục hoặc lưỡi mác, khá là mỏng và mọc đối nhau. Lúc cây sở hữu tuổi thọ khoảng 30 năm trở lên sẽ cho chất lượng trầm tương đối thấp. Cái cây này hơi dễ trồng nhưng lại sở hữu tốc độ sinh trưởng chậm, đấy cũng là 1 phần lý do khiến nó đắt giá như hiện nay.

https://tramhuongangiabao.com.vn/

https://gab.com/tramhuonggiabao

https://www.deviantart.com/tramhuonggiabao

https://www.twitch.tv/tramhuonggiabao/about
